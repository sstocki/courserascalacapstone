package observatory {

  case class Location(lat: Double, lon: Double)

  case class Color(red: Int, green: Int, blue: Int)

  case class StationRow(stn: String, wban: Option[String], lat: Option[Double], lon: Option[Double])

  case class TemperatureRow(stn: String, wban: Option[String], month: Int, day: Int, temp: Double)
}